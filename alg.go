package main

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
)

var gene_pool []Gene
var gene_pool_mutex sync.RWMutex
var cities_to_visit []int
var num_cities_to_visit int

func initGene() {
	cities_to_visit = removeFromSlice(start_point, getKeysFromMap(CR.to_name))
	num_cities_to_visit = len(cities_to_visit)
	fmt.Println(num_cities_to_visit, cities_to_visit)
}

type Gene struct {
	keys  []int
	price int
  have_visited map[int]struct{}
}

func (g Gene) String() string {
	cities := []string{CR.get_name(start_point)}
	for _, city := range g.keys {
		cities = append(cities, CR.get_name(city))
	}
	return fmt.Sprintf(strings.Join([]string{strings.Join(cities, " -> "), strconv.Itoa(g.price)}, " Price: "))
}

func (g *Gene) is_valid() bool {
	// Have the gene all the cities
	for value := range CR.to_name {
		if intInSlice(value, g.keys) == false {
			return false
		}
	}
	return true
}

func (g *Gene) add_key(key int, price int) Gene {
	new_g := Gene{}
	new_g.keys = append(g.keys, key)
	new_g.price = g.price + price
  new_g.have_visited = make(map[int]struct{})
	for key, _ := range g.have_visited {
		new_g.have_visited[key] = struct{}{}
	}
	return new_g
}

func (g *Gene) visited(key int) bool {
  _, ok := g.have_visited[key]
  return ok
}

func generate_gene(g Gene) {
	nday := len(g.keys)
	//fmt.Println(nday, g)
	day := data.get_day(nday)
	var city *City
	if nday == 0 {
		city = day.get_city(start_point)
	} else {
		city = day.get_city(g.keys[nday-1])
	}
	if nday == num_cities_to_visit {
		cost, ok := city.flights[start_point]
		if !ok {
			return
		}
		out_g := g.add_key(start_point, cost)
		gene_pool_mutex.Lock()
		defer gene_pool_mutex.Unlock()
		gene_pool = append(gene_pool, out_g)
		//fmt.Println("ADDING TO GENEPOOL", out_g)
		return
	}
	if city == nil {
		//fmt.Println("NOCITY", nday)
		return
	}
	//fmt.Println(city.flights)
	for to_city, price := range city.flights {
		if !intInSlice(to_city, g.keys) && to_city != start_point {
			tmp := g.add_key(to_city, price)
			//fmt.Println(tmp)
			generate_gene(tmp)
		}
	}

}

func gen_city_genes(send <-chan Gene, result chan<- Gene, city *City, city_id int) {
	var genes []Gene
	var genes_left []Gene
	var out_genes []Gene
	for g := range send {
		genes = append(genes, g)
	}
  genes_left = make([]Gene, len(genes))
  copy(genes_left, genes)
	//fmt.Println("-----------------------------", city_id, genes)
	for to_city, price := range city.flights {
		found := false
		for i, g := range genes_left {
			if !g.visited(to_city) && to_city != start_point {
				found = true
				genes_left = append(genes_left[:i], genes_left[i+1:]...)
				out_genes = append(out_genes, g.add_key(to_city, price))
				break
			}
		}
		if !found { // No more original genes left we have to replicate some of them
			for _, g := range genes {
				if !g.visited(to_city) && to_city != start_point {
					out_genes = append(out_genes, g.add_key(to_city, price))
					break
				}
			}
		}
	}
	for _, g := range genes_left { // There are more genes than possible roads
		for to_city, price := range city.flights {
			if !g.visited(to_city) && to_city != start_point {
				out_genes = append(out_genes, g.add_key(to_city, price))
				break
			}
		}
	}
	for _, g := range out_genes {
		result <- g
	}
	close(result)
}

func generate_gene_pool() {
	var gene_pool_now []Gene
	empty_base_gene := Gene{}
	day := data.get_day(0)
	city := day.get_city(start_point)
	for to_city, price := range city.flights {
		gene_pool_now = append(gene_pool_now, empty_base_gene.add_key(to_city, price))
	}
	for day_n := 1; day_n <= num_cities_to_visit-1; day_n++ {
		day := data.get_day(day_n)
		send_channels := make(map[int]chan Gene)
		var rec_channels []chan Gene
		for _, city := range cities_to_visit {
			s_channel := make(chan Gene)
			r_channel := make(chan Gene)
			c := day.get_city(city)
			go gen_city_genes(s_channel, r_channel, c, city)
			send_channels[city] = s_channel
			rec_channels = append(rec_channels, r_channel)
		}
		for _, g := range gene_pool_now { // Filter cities
			send_channels[g.keys[day_n-1]] <- g
		}

		for _, ch := range send_channels {
			close(ch) // signalize there is no more cities
		}
		gene_pool_now = nil
		for _, ch := range rec_channels {
			for g := range ch {
				gene_pool_now = append(gene_pool_now, g) // collect results
			}
		}
	}
	fmt.Println("Num of genes:", len(gene_pool_now))
}
