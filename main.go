package main

import (
	"fmt"
	"runtime"
)

var Version string
var BuildTime string
var data Database
var CR city_register
var start_point int

const numCPU = 4
const numParseWorkers = numCPU * 2
const numParseChannels = 1000

func main() {
	fmt.Println("Version:", Version, "Build time:", BuildTime) // THIS CAN'T BE IN OUTPUT
	CR = NewCityRegister()
	runtime.GOMAXPROCS(numCPU) // Run on 4 cores

	data.days = make(map[int]*Day)

	toc := Timeit("PARSE")
	start := parse()
	toc()
	start_point = CR.get_id(start)

	initGene()
	toc = Timeit("GEN GENE POOL")
	generate_gene_pool()
	toc()

	fmt.Println(len(gene_pool))
}
