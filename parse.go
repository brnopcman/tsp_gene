package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

func worker(wg *sync.WaitGroup, lines <-chan string) {
	for l := range lines {
		parse_line(l)
	}
	wg.Done()
}

func parse_line(line string) {
	from_city_str, to_city_str, day, cost := split_line(line)
	from_city := CR.get_or_create_id(from_city_str)
	to_city := CR.get_or_create_id(to_city_str)
	data.add_flight(day, from_city, to_city, cost)
}

func split_line(line string) (string, string, int, int) {
	// return from_city, to_city, day, cost
	s := strings.Split(line, " ")
	day, _ := strconv.Atoi(s[2])
	cost, _ := strconv.Atoi(s[3])
	return s[0], s[1], day, cost
}

func parse() string {
	wg := &sync.WaitGroup{}
	lines := make(chan string, numParseChannels)

	reader := bufio.NewReader(os.Stdin)
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)

	scanner.Scan() // Look for first line
	start := scanner.Text()

	// Spawn Workers
	for w := 0; w < numParseWorkers; w++ {
		wg.Add(1)
		go worker(wg, lines)
	}

	n := 0
	for scanner.Scan() {
		lines <- scanner.Text()
		n++
	}
	fmt.Println(n)
	close(lines)
	wg.Wait()
	return start
}
