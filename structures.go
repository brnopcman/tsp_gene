package main

import (
	"sync"
)

// ####################################### city_register #######################################

type city_register struct {
	city_checker map[string]struct{} // Set of names of cities
	to_id        map[string]int
	to_name      map[int]string
	max_id       int
	mutex        sync.RWMutex
}

func NewCityRegister() city_register {
	var cr city_register
	cr.to_id = make(map[string]int)
	cr.to_name = make(map[int]string)
	cr.max_id = 0 // 0 means does not exist/empty
	return cr
}

func (cr *city_register) contains(s string) bool {
	cr.mutex.RLock()
	defer cr.mutex.RUnlock()
	return cr.to_id[s] != 0
}

func (cr *city_register) __add_city(s string) int {
	// INTERNAL add city do not check if exists
	cr.mutex.Lock() // Only one can add new city at once
	defer cr.mutex.Unlock()
	id := cr.to_id[s]
	if id == 0 {
		id = cr.max_id + 1
		cr.max_id += 1
		cr.to_id[s] = id
		cr.to_name[id] = s
	}
	return id
}

func (cr *city_register) add_city(s string) {
	// add city if not exists
	if cr.contains(s) {
		return
	} // city is already in register
	cr.__add_city(s)
}

func (cr *city_register) get_id(s string) int {
	cr.mutex.RLock()
	defer cr.mutex.RUnlock()
	return cr.to_id[s]
}

func (cr *city_register) get_name(id int) string {
	cr.mutex.RLock()
	defer cr.mutex.RUnlock()
	return cr.to_name[id]
}

func (cr *city_register) get_or_create_id(s string) int {
	// return id of city if not exists creat it and return id
	if id := cr.get_id(s); id == 0 {
		return cr.__add_city(s)
	} else {
		return id
	}
}

func (cr *city_register) get_city_list(without int) []int {
	keys := make([]int, len(cr.to_name))
	i := 0
	for k := range cr.to_name {
		keys[i] = k
		i++
	}
	return keys
}

// ####################################### database #######################################

type City struct {
	flights map[int]int
	mutex   sync.RWMutex
}

func (c City) copy() *City {
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	new_c := new(City)
	new_c.flights = make(map[int]int)
	for key, val := range c.flights {
		new_c.flights[key] = val
	}
	return new_c
}

func (c *City) add_flight(city int, cost int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.flights[city] = cost

}

func (c *City) get_flight(city int) int {
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	return c.flights[city]
}

type Day struct {
	cities map[int]*City
	mutex  sync.RWMutex
}

func (d *Day) copy() Day {
	d.mutex.RLock()
	defer d.mutex.RUnlock()
	new_d := Day{}
	new_d.cities = make(map[int]*City)
	for key, val := range d.cities {
		new_d.cities[key] = val.copy()
	}
	return new_d
}

func (d *Day) get_city(city int) *City {
	d.mutex.RLock()
	defer d.mutex.RUnlock()
	return d.cities[city]
}

func (d *Day) create_city(city int) *City {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	val, ok := d.cities[city]
	if !ok {
		val = new(City)
		val.flights = make(map[int]int)
		d.cities[city] = val
	}
	return val
}

func (d *Day) get_or_create_city(city int) *City {
	if val := d.get_city(city); val == nil {
		return d.create_city(city)
	} else {
		return val
	}
}

type Database struct {
	days  map[int]*Day
	mutex sync.RWMutex
}

func (d *Database) get_day(day int) *Day {
	d.mutex.RLock()
	defer d.mutex.RUnlock()
	return d.days[day]
}

func (d *Database) create_day(day int) *Day {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	val, ok := d.days[day]
	if !ok {
		val = new(Day)
		val.cities = make(map[int]*City)
		d.days[day] = val
	}
	return val
}

func (d *Database) get_or_create_day(day int) *Day {
	if val := d.get_day(day); val == nil {
		return d.create_day(day)
	} else {
		return val
	}
}

func (d *Database) add_flight(day int, from_city int, to_city int, cost int) {
	fday := d.get_or_create_day(day)
	fcity := fday.get_or_create_city(from_city)
	fcity.add_flight(to_city, cost)
}
